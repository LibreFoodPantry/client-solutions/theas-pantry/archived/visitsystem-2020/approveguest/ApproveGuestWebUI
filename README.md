# Approve Guest Web UI

Main UI for admin approval UI
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Requirement
- Node.js (v12 or greater)

## Initial installation
- Update the lastest version of NPM with `npm install npm@latest -g`
- Run `npm install` to get all the dependencies installed

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Documentation

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/StandardProject/)
- [Edit on GitLab](docs) (in `docs/` directory)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
