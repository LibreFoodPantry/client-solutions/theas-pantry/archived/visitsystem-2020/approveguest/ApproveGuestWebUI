import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { Guest} from '../../models/Guest';
import {MatTableDataSource} from '@angular/material/table';
import {GuestService} from '../services/guest.service';
import {MatSort} from '@angular/material/sort';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-guest-list',
  templateUrl: './guest-list.component.html',
  styleUrls: ['./guest-list.component.css']
})
export class GuestListComponent implements OnInit, AfterViewInit {
  displayedColumns = ['id', 'size', 'date', 'status', 'weight'];
  currentGuest: Guest;
  dataSource = new MatTableDataSource<Guest>();
  selectedRow: number = -1;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private guestService: GuestService, private  dataService: DataService) { }

  ngOnInit(): void {
   this.guestService.getGuests().subscribe(guestArray => {
     this.dataSource.data = guestArray;
    }, (error) => {
     console.log(error);
   });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  editGuest(guest: Guest) {
    this.currentGuest = guest;
    this.dataService.changeSelected(guest);
    this.selectedRow = parseInt(guest.id);
  }

  approveAll() {
    this.dataSource.data.forEach((guest) => {
        guest.status = true;
        guest.weight = this.getWeight(guest.size);
    });
  }

  // returns weight that customer is approved for based on household size
  getWeight(householdSize: number): number {
    if (householdSize === 1) {
      return 30;
    } else if (householdSize > 1) {
      return 50;
    } else {
      return -1;
    }
  }

}
