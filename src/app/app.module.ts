import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { GuestInfoComponent } from './guest-info-component/guest-info.component';
import { GuestService } from './services/guest.service';
import { DataService } from './services/data.service';
import { GuestListComponent } from './guest-list/guest-list.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { HeaderComponent } from "./header/header.component";



@NgModule({
  declarations: [
    AppComponent,
    GuestInfoComponent,
    GuestListComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    ),
  ],
  providers: [HttpClient, GuestService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
