import { Component, OnInit, Input } from '@angular/core';
import { GuestService } from '../services/guest.service';
import { Guest } from '../../models/Guest';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-guest-info-component',
  templateUrl: './guest-info.component.html',
  styleUrls: ['./guest-info.component.css']
})

export class GuestInfoComponent implements OnInit {


  constructor(private guestService: GuestService, private dataService: DataService) {
  }

  currentGuest: Guest;

  ngOnInit(): void {
    this.dataService.currentSelectedGuest.subscribe(guest => this.currentGuest = guest);
  }



}

