import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Guest} from '../../models/Guest';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // Default Values for BehaviorSubject
  private guestSource = new BehaviorSubject<Guest>({
    date: '', status: false,
    id: '',
    size: null, weight: 0

  });
  currentSelectedGuest: Observable<Guest> = this.guestSource.asObservable();
  constructor() { }

  // Updates current guest in DataService
  changeSelected(guest: Guest) {
    this.guestSource.next(guest);
  }
}
