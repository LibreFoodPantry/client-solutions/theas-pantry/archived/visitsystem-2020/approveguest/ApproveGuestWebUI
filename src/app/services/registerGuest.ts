import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Guest } from '../../models/Guest';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RegisterGuest {
  registerUrl = 'localhost:8080/registerGuest';
  constructor(private http: HttpClient) { }

  // get all registered Guests
  getRegisteredGuests(): Observable<Guest[]> {
    const url = `${this.registerUrl}/allRegistration`;
    return this.http.get<Guest[]>(url);
  }

  // get Registered Guest by id
  getRegisteredGuest(id: number): Observable<Guest> {
    const url = `${this.registerUrl}/getRegistration/${id}`;
    return this.http.get<Guest>(url);
  }

}
