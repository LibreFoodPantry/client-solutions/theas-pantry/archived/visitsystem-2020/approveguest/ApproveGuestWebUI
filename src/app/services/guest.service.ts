import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Guest } from '../../models/Guest';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class GuestService {
  approveUrl = 'api/guests';
  constructor(private http: HttpClient) { }

  // get Guests
  getGuests(): Observable<Guest[]> {
    return this.http.get<Guest[]>(this.approveUrl);
  }

  // get Guest by id
  getGuest(id: number): Observable<Guest> {
    const url = `${this.approveUrl}/${id}`;
    return this.http.get<Guest>(url);
  }

  // update Guest Approval
  approveUpdate(guest: Guest): Observable<any> {
    const url = `${this.approveUrl}/approveUpdate/`;
    return this.http.post(url, guest, httpOptions);
  }
}
