import {InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const guests = [
      {id: 1, size: 5, date: '11/12/2001', status: false, weight: -1},
      {id: 2, size: 1, date: '12/09/2015', status: false, weight: -1},
      {id: 3, size: 2, date: '09/21/2009', status: false, weight: -1},
      {id: 4, size: 6, date: '07/15/2018', status: false, weight: -1},
      {id: 5, size: 4, date: '01/22/2012', status: false, weight: -1},
      {id: 6, size: 3, date: '12/09/2015', status: false, weight: -1},
      {id: 7, size: 1, date: '09/21/2009', status: false, weight: -1},
      {id: 8, size: 6, date: '07/15/2018', status: false, weight: -1},
      {id: 9, size: 4, date: '01/22/2012', status: false, weight: -1}
    ];
    return {guests};
  }
  constructor() { }
}
