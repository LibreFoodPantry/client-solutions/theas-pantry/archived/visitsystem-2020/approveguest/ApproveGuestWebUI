export class Guest {
  id: string;
  size: number;
  date: string;
  status: boolean;
  weight: number;
}
